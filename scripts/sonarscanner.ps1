Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"
$PSDefaultParameterValues['*:ErrorAction'] = 'Stop'

#& dotnet new tool-manifest
#& dotnet tool install --global dotnet-sonarscanner
#& nuget restore -NoCache  # restore Nuget dependencies
if ([string]::IsNullOrEmpty("$env:CI_MERGE_REQUEST_IID"))
{
    & echo "Running Branch analysis"
    & dotnet sonarscanner begin `
          /o:"march4dotnet8" `
          /k:"march4dotnet8_dotnetcoreapp" `
          /d:sonar.login="63ec036a8b11eb851b490d42c330428b001977ef" `
          /d:sonar.host.url="https://sonarcloud.io" `
          /d:sonar.qualitygate.wait=true `
          /d:sonar.branch.name="$env:CI_COMMIT_BRANCH"
}
else
{
    & echo "Running SQ Merge Request analysis"
    & dotnet sonarscanner begin `
          /o:"march4dotnet8" `
          /k:"march4dotnet8_dotnetcoreapp" `
          /d:sonar.login="63ec036a8b11eb851b490d42c330428b001977ef" `
          /d:sonar.host.url="https://sonarcloud.io" `
          /d:sonar.qualitygate.wait=true `
          /d:sonar.pullrequest.key="$env:CI_MERGE_REQUEST_IID" `
          /d:sonar.pullrequest.branch="$env:CI_MERGE_REQUEST_SOURCE_BRANCH_NAME" `
          /d:sonar.pullrequest.base="$env:CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
}

& "$env:MSBUILD_PATH" `
    .\sampledotnetapp1\sampledotnetapp1.csproj -t:Build  # build the project
& dotnet sonarscanner end `
    /d:sonar.login="63ec036a8b11eb851b490d42c330428b001977ef"